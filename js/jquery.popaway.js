(function ( $ ) {

  // function to get the center position for the particular element. this function is public
  function getCenter(element)
    {
      var winDimensions = {"height":$(window).height(), "width":$(window).width()},
          // popup element's dimensions
          targetDimensions = {"height":element.outerHeight(), "width":element.outerWidth()},
          // popup element's center position
          centerPosition = {"top":(winDimensions.height-targetDimensions.height)/2,"left":(winDimensions.width-targetDimensions.width)/2};
      
      return centerPosition;
    }
  // global configuration object. dimmer, dimmer color, dimmer opacity and if multiple instances can be opened at the same time
  var globalConfig = {dimmer:true,dimColor:"#000",dimOpacity:0.5,multiple:false},
      dimmerStyle = {"position":"fixed","width":"100%","height":"100%","background":globalConfig.dimColor,"opacity":"0","visibility":"hidden","top":"0","left":"0","z-index":"5000"},
      dimmerEl,dimmerTween;

  // this function 
  function parseDimStyles()
  {
    dimmerEl.css({"background":globalConfig.dimColor});
  }
	// because of code execution the document is not ready before the plugin is loaded, therefore the dimmer is added after.
  $(document).ready(function()
    {
      var dimmer = $("<div />",{id:"popAway-dimmer"}).css(dimmerStyle);
      
      $("body").append(dimmer);
      
      // dimmer jquery object 
      dimmerEl = $("#popAway-dimmer");
    });

	$.fn.popAway = function( tweenTime, fromSett, toSett )
  {
    // this array contains the strings of the most common properties animated and the ones that will be supported in the plugin. this will be used if only starting values are passed in the function. those params will be compared to the ones in this and the end values config object will be set up according to default values.
    var propertiesArray = ["left","top", "rotation", "rotationX", "rotationY", "x", "y", "z", "skewX", "skewY"],
        target = this,
        duration = tweenTime,
        startValues,endValues,t;

    // add the corresponding class to the element and since the popup starts closed add that class too
    this.addClass("popAway popAway-closed").css("z-index","5100");

    // store the element's original position, in case the popup has to be reseted
    this[0].originalTop = this.offset().top;
    this[0].originalLeft = this.offset().left;
    // store this particular instance's duration. This is used when the window is resized
    this[0].popAwayDuration = tweenTime;

    return this.each(function()
    {
      // if the window is being resized
      if(fromSett === "resize")
      {
        if( $(this).hasClass("pop-from-center") )
        {
          var centerPoint = getCenter( $(this) ),currentProgress, t;

          currentProgress = this.popAway.progress();

          // this tween is comming from the screen center, change just the from params
          this.fromParams.left = centerPoint.left;
          this.fromParams.top = centerPoint.top;
          this.fromParams.autoAlpha = 0;

          // set the initial and final config objects of the tween
          startValues = {css:this.fromParams};
          endValues = {css:this.toParams};

          // create the tween again
          t = TweenLite.fromTo(this, this.popAwayDuration, startValues, endValues);
          t.progress(  currentProgress );
          t.pause();
          this.popAway = t;

        } // pop from center conditional end

        // if the popup is being tweened to the center of the screen
        else if( $(this).hasClass("pop-to-center") )
        {
          var centerPoint = getCenter( $(this) ),currentProgress, t;

          currentProgress = this.popAway.progress();

          // change the position properties of the config object
          this.toParams.left = centerPoint.left;
          this.toParams.top = centerPoint.top;

          // set the initial and final config objects of the tween
          startValues = {css:this.fromParams};
          endValues = {css:this.toParams};

          // create the tween again
          t = TweenLite.fromTo(this, tweenTime, startValues, endValues);
          t.progress(  currentProgress );
          t.pause();
          this.popAway = t;          
        }
        // stop code execution
        return false;
      }// resize conditional end

      // if both start and end values are not passed use a default scale and opacity effect on the element. The element's position is not changed.
      if(!fromSett && !toSett)
      {
        t = TweenLite.fromTo(target, tweenTime, {scale:0.001, autoAlpha:0},{scale:1, autoAlpha:1, force3D:true});
      }

      // if the starting and end values are given by the user simply create a fromTo() instance with those values
      if(fromSett && toSett)
      {
        // if in the starting values position:"center" is passed the element's initial top and left correspond to the center position and it'll end in the values passed in the landing values. other values can be added in order to give a more complete animation.
        if(fromSett.position)
        {
          // this variable will hold the center position of the target element in case is needed
          var centerPoint = getCenter(target);

          // add a specific class to the element. this is important for the resize event, that also could be a device orientation change.
          target.addClass("pop-from-center");

          // now remove the position property of the config object passed. this is no longer necessary because a class name will be added to the element for easier selection later
          delete fromSett.position;

          // add the defaults key:value pairs for this setting. first the left and top positions and then the autoAlpha property
          fromSett.left = centerPoint.left;
          fromSett.top = centerPoint.top;
          fromSett.autoAlpha = 0;
          // add the autoAlpha property value to the landing values config object
          toSett.autoAlpha = 1;
          toSett.force3D = true;

          // attach the starting values to the DOM element
          target[0].fromParams = fromSett;
          // attach the end values to the DOM element
          target[0].toParams = toSett;

          startValues = {css:fromSett};
          endValues = {css:toSett};
        } // conditional for starting value equals "center" end

        // if instead of a config object the string "center" is passed then the popup will go from the values passed in the starting config object to the center of the screen.
        if(toSett === "center")
        {
          // center the target. the animation is relaticve to the center position not the original rendered position
          centerEl(target,0);

          // this variable will hold the center position of the target element in case is needed
          var centerPoint = getCenter(target),
              newObj = {};

          // add a specific class to the element. this is important for the resize event, that also could be a device orientation change.
          target.addClass("pop-to-center");

          // add the defaults properties for this setting. first the left and top positions and then autoAlpha
          newObj.left = centerPoint.left;
          newObj.top = centerPoint.top;
          newObj.autoAlpha = 1;
          newObj.force3D = true;

          // copy the properties in the starting values and set default final values
          for(var i in fromSett)
          {
            if(i === "left" || i === "top"){}
            else
            {
              newObj[i] = 0;
            }
          }

          // add the autoAlpha to the starting values
          fromSett.autoAlpha = 0;

          // attach the starting values to the DOM element
          target[0].fromParams = fromSett;
          // attach the end values to the DOM element
          target[0].toParams = newObj;

          startValues = {css:fromSett};
          endValues = {css:newObj};
        } // conditional for starting values "center" end

        // if particular values are passed in both config objects, is just a regular fromTo animation.
        else
        {
          // set the starting autoAlpha
          fromSett.autoAlpha = 0;
          // set the end autoAlpha
          toSett.autoAlpha = 1;
          toSett.force3D = true;
          // attach the starting values to the DOM element
          target[0].fromParams = fromSett;
          // attach the end values to the DOM element
          target[0].toParams = toSett;

          startValues = {css:fromSett};
          endValues = {css:toSett};
        }
        t = TweenLite.fromTo(target, tweenTime, startValues, endValues);
      }

      // if only starting values are passed. copy the properties used in the starting values and change their values to defaults as the end values. in this case the element will be animated to it's position when the tween was created. this gives the user the ability to animate the element to the position of her/his choice established in the CSS
      if(fromSett && !toSett)
      {
        if(fromSett === "center")
        {
          // center the target. the animation is relative to the center position not the original rendered position
          centerEl(target,0);
          // add a class to the element
          target.addClass("pop-away-center");
          t = TweenLite.fromTo(target, tweenTime, {scale:0.001, autoAlpha:0},{scale:1, autoAlpha:1, force3D:true});
        }

        // if position:"center" is passed in the starting config object and no final config object is passed, the element should be animated from the screen center to the rendering position. the rest of the properties will be set to 0 in the ending config object.
        else if(fromSett.position)
        {
          // add a specific class to the element. this is important for the resize event, that also could be a device orientation change.
          target.addClass("pop-from-center");

          var centerPoint = getCenter(target),
              targetOffset = target.offset();

          for(var i in fromSett)
          {
            if(i != "position")
            {
              targetOffset[i] = 0;
            }
          }

          // add the autoAlpha property to the starting config object. in this case the animation starts at the center so the object is centerPoint
          fromSett.autoAlpha = 0;
          fromSett.left = centerPoint.left;
          fromSett.top =centerPoint.top;

          // add the autoAlpha to the final config object. the animation ends in the original element's position so the object is targetOffset
          targetOffset.autoAlpha = 1;
          targetOffset.force3D = true;

          // attach the starting values to the DOM element
          target[0].fromParams = centerPoint;
          // attach the end values to the DOM element
          target[0].toParams = targetOffset;

          startValues = {css:fromSett};
          endValues = {css:targetOffset};

          t = TweenLite.fromTo(target, tweenTime, startValues, endValues);
        }

        else
        {
          var targetOffset = target.offset(),
              newObj = {};
          
          // add the default starting autoAlpha to the config object
          fromSett.autoAlpha = 0;
          
          // loop through all the properties of the config object in order to create the landing values config object
          for(var i in fromSett)
          {
            if(i === "left" || i === "top"){};
            // 
            newObj[i] = 0;      
          }// fromSett object loop end

          // add the default final autoAlpha value
          newObj.autoAlpha = 1;
          newObj.force3D = true;

          // attach the starting values to the DOM element
          target[0].fromParams = fromSett;
          // attach the end values to the DOM element
          target[0].toParams = newObj;

          // store the objects in GSAP syntax for fastest use
          startValues = {css:fromSett};
          endValues = {css:newObj};

          t = TweenLite.fromTo(target, tweenTime, startValues, endValues);
        }
        
      }

      t.pause();
      this.popAway = t;
    }); // return this.each end
    
  }; // fn.popAway end

  function centerEl(element, time)
  {
    console.log("center!!!");
    var centerPosition = getCenter(element);
    TweenLite.to(element, time, {top:centerPosition.top, left:centerPosition.left});
    element[0].initPosition = element[0].getBoundingClientRect();
  }

  $(window).resize(function()
    {
      var popUpEls = $(".popAway");

      $.each(popUpEls, function(i,e)
      {
        // if the element is a simple center animated popup with default animation
        if($(e).hasClass("pop-away-center"))
        {
          centerEl($(e), 0.2);
        }
        // if the element has any of the classes that comes from a center instance
        else if($(e).hasClass("pop-from-center") || $(e).hasClass("pop-to-center"))
        {
          // search for active tweens. this in case that the tween is playing/reversing during the window resize
          var activeTweens = e.popAway.isActive(),
              activeDuration = e.popAway.duration(),
              waitTime = 0,
              activeTime, activeState;

          // if the popup tween is active
          if(activeTweens)
          {
            // active tween current time
            activeTime = e.popAway.time();
            // active tween reversed state
            activeState = e.popAway.reversed();

            // how long before the function to create the tween again has to pass. This to avoid overwrite confilcts
            // if the tween is going forward
            if(!activeState)
            {
              waitTime = activeDuration - activeTime;
            }
            // if the tween is going back
            else
            {
              waitTime = activeTime;
            }
          }//active tween conditional end
          // execute the popup function passing the resize string after the tween is completed. If the tween is not active, the wait time is zero so this can be used in any circumstance
          TweenLite.delayedCall(waitTime, function()
            {
              $(e).popAway(e.popAwayDuration,"resize");
            });
        }// conditional class check

      });// each loop end
    });// window resize end

  popAway = {};

  popAway.configOp = function(options)
  {
    if(options)
    {
      for(var i in options)
      {
        //console.log(i + " --- " + options[i]);
        globalConfig[i] = options[i];
      }
    }
    //console.log( "dimmer => " + globalConfig.dimmer + " /// dimmer color => " + globalConfig.dimColor + " /// dimmer opacity => " + globalConfig.dimOpacity + " /// multiple instances => " + globalConfig.multiple );
    // call a function to apply the styles to the dimmer
    parseDimStyles();
  };

  popAway.popOpen = function(target)
  {
    target.removeClass("popAway-closed").addClass("popAway-opened");
    target[0].popAway.play();
    TweenLite.to(dimmerEl, 0.1,{autoAlpha:globalConfig.dimOpacity});
  };

  popAway.popClose = function(target)
  {
    target.removeClass("popAway-opened").addClass("popAway-closed");
    TweenLite.to(dimmerEl, 0.1,{autoAlpha:0});
    target[0].popAway.reverse();
  };
		
}( jQuery ));
